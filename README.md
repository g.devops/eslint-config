# @g.devops/eslint-config

ESLint configs for contributing to @guischdi projects

- [base](#Base)
- [react](#React)
- tbc.

## Installation

`npm i -D @g.devops/eslint-config eslint`

## Setup

### Base

Add these lines to your `package.json`:
```json
  "eslintConfig": {
    "extends": "@g.devops/eslint-config"
  }
```

### React

Additionally install the react-app eslint config:
```
npm i -D eslint-config-react-app
```

Add these lines to your `package.json`:
```json
  "eslintConfig": {
    "extends": "@g.devops/eslint-config/react"
  }
```

## License

MIT
