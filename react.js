'use strict';

module.exports = {
  extends: [
    'react-app',
    './index.js',
  ],
  parserOptions: {
    sourceType: 'module',
  },
};
